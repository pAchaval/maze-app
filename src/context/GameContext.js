import createDataContext from "./createDataContext";
import gameServer from '../api';
import { filterAndSortResponse } from "../helpers";


const gameReducer = (state, action) => {
    switch (action.type) {
        case "SET_WINNER":
            return {
                ...state,
                winner: action.payload,
            }
        case "SET_NAME":
            return {
                ...state,
                name: action.payload,
            }
        case "SET_MOVES":
            return {
                ...state,
                moves: action.payload,
            }
        case "SET_RANKINGS":
            return {
                ...state,
                rankings: action.payload,
            }
        case "SET_ERROR":
            return {
                ...state,
                error: action.payload,
            }
        default:
            break;
    }
}

const getRankings = dispatch => {
    return async (level) => {
        await gameServer.get('/rankings')
            .then((response) => dispatch({ type: 'SET_RANKINGS', payload: filterAndSortResponse(response.data, level) }))
            .catch(() => dispatch({ type: 'SET_ERROR', payload: 'Error getting ranking' }))
    }
}

const submitResults = dispatch => {
    return async (name, moves, level) => {
        await gameServer.post('/rankings', { name, moves, level })
            .catch(() => dispatch({ type: 'SET_ERROR', payload: 'Error posting score' }))
    }
}

const restartGame = dispatch => () => {
    dispatch({ type: 'SET_MOVES', payload: 0 });
    dispatch({ type: 'SET_NAME', payload: '' });
    dispatch({ type: 'SET_WINNER', payload: false });
}

const setMoves = dispatch => (moves) => {
    dispatch({ type: 'SET_MOVES', payload: moves })
}

const setWinner = dispatch => (isWinner) => {
    dispatch({ type: 'SET_WINNER', payload: isWinner })
}

const submitSetUp = dispatch => (name) => {
    dispatch({ type: 'SET_NAME', payload: name })
}

export const { Provider, Context } = createDataContext(
    gameReducer,
    { setWinner, submitSetUp, getRankings, restartGame, setMoves, submitResults },
    {
        winner: false,
        name: '',
        rankings: [],
        moves: 0,
        error: '',
    }
)
