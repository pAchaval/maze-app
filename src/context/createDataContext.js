/* eslint-disable import/no-anonymous-default-export */
import React, { useReducer } from 'react';

export default (reducer, actions, defaultValue) => {
    const Context = React.createContext();

    const Provider = ({ children }) => {
        const [state, dispatch] = useReducer(reducer, defaultValue);

        // asociate dispatch to every action so as to make the function available to the component
        // instead of dispatching directly from it
        const boundActions = {};
        for (let key in actions) {
            boundActions[key] = actions[key](dispatch);
        }

        return (
            // make state and functions (actions with dispatch) available throguh context
            <Context.Provider value={{ state, ...boundActions }}>
                {children}
            </Context.Provider>
        );
    };

    return { Context: Context, Provider: Provider };
};
