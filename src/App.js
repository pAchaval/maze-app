import './App.css';
import Game from './components/Game';
import { Provider } from './context/GameContext';

function App() {
  return (
    <Provider>
      <Game />
    </Provider>
  )
}

export default App;
