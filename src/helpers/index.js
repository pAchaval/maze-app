import { KEY_PRESSED } from "../constants";

export function validateAndMove(e, position, maze) {
    const columns = position[0];
    const rows = position[1];

    switch (e.keyCode) {
        case KEY_PRESSED.UP:
            // preventing the window to scroll if this specific key is pressed
            e.preventDefault();
            if (maze[columns - 1][rows] === 1 || maze[columns - 1][rows] === 2 || maze[columns - 1][rows] === 3) {
                return [(columns - 1), rows];
            }
            return position;


        case KEY_PRESSED.DOWN:
            // preventing the window to scroll if this specific key is pressed
            e.preventDefault();
            if (maze[columns + 1][rows] === 1 || maze[columns + 1][rows] === 2 || maze[columns + 1][rows] === 3) {
                return [(columns + 1), rows];
            }
            return position;

        case KEY_PRESSED.RIGHT:
            // preventing the window to scroll if this specific key is pressed
            e.preventDefault();
            if (maze[columns][rows + 1] === 1 || maze[columns][rows + 1] === 2 || maze[columns][rows + 1] === 3) {
                return [columns, (rows + 1)];
            }
            return position;

        case KEY_PRESSED.LEFT:
            // preventing the window to scroll if this specific key is pressed
            e.preventDefault();
            if ((maze[columns][rows - 1] === 1) || (maze[columns][rows - 1] === 2) || (maze[columns][rows - 1] === 3)) {
                return [columns, (rows - 1)];
            }
            return position;

        default:
            return position;
    }
}

export function compareCoords(coordsA, coordsB) {
    if (coordsA.toString() === coordsB.toString()) return true;
    return false;
}

export function filterAndSortResponse(array, level) {

    // filter array by 'level' prop, to only see the results for the ongoing level
    let filteredArray = array.filter((obj) => obj.level === level);

    // Sort by moves in ascending order (less moves better ranking)
    return filteredArray.sort((a, b) => {
        if (a.moves > b.moves) {
            return 1;
        }
        if (a.moves < b.moves) {
            return -1;
        }
        return 0;
    });
};

export function searchMazeStartEnd(maze, startEndRef) {
    for (let i = 0; i < maze.length; i++) {
        for (let j = 0; j < maze[i].length; j++) {
            if (maze[i][j] === startEndRef) {
                return [i, j]
            }
        }
    }
}
