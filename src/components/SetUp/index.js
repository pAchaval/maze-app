import React, { useContext } from "react";
import { useState } from "react/cjs/react.development";
import { INPUT_PLACEHOLDER, SUBMIT_SETUP_BUTTON } from "../../constants";
import { Context as GameContext } from "../../context/GameContext";
import Button from "../Button";
import Input from "../Input";

const SetUp = () => {
    const { submitSetUp } = useContext(GameContext);
    const [name, setName] = useState('');

    return (
        <div className="setup">
            <Input className='name-input' placeholder={INPUT_PLACEHOLDER} action={(e) => setName(e.target.value)} value={name} />
            <Button className='setup-button' disabled={!name} action={() => submitSetUp(name)} label={SUBMIT_SETUP_BUTTON} />
        </div>
    )
}

export default SetUp;
