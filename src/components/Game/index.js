/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from "react";
import { EASY_MAZE, FINISHING_POINT_REF, GAME_LEVELS, GAME_NAME, HARD_MAZE, STARTING_POINT_REF, SUBMIT_GAME_BUTTON } from "../../constants";
import { Context as GameContext } from "../../context/GameContext";
import { compareCoords, searchMazeStartEnd, validateAndMove } from "../../helpers";
import Button from "../Button";
import Header from "../Header";
import InformationLabel from "../InformationLabel";
import Maze from "../Maze";
import Rankings from "../Rankings";
import SetUp from "../SetUp";

const Game = () => {
    const { state, setWinner, getRankings, setMoves, restartGame, submitResults } = useContext(GameContext);

    // initializing easy maze, there is an option for HARD as well
    // CHANGE THIS CONSTANT TO ACCESS THE OTHER LEVEL
    const level = GAME_LEVELS.EASY;
    const maze = level === GAME_LEVELS.EASY ? EASY_MAZE : HARD_MAZE;

    // calculate winning coordinates looping through the 2D array looking for the number 3
    const winningCoords = searchMazeStartEnd(maze, FINISHING_POINT_REF);

    // calculate initial position looping through the 2D array looking for the number 2
    // positioning the character in that set of coordinates
    const [position, setPosition] = useState(searchMazeStartEnd(maze, STARTING_POINT_REF));

    useEffect(() => {
        // use effect without dependencies used as an onMount to add key listener
        window.addEventListener("keydown", move, false);
        return () => {
            // remove listener on unMount
            window.removeEventListener("keydown", move, false);
        }
    })

    // Fetch rankings when a name is defined
    // name is used to detect if we are on setup instance or on game instance
    useEffect(() => {
        state.name && getRankings(level);
    }, [state.name])

    const move = (e) => {
        // disable movement after winning
        if (!state.winner) {
            // validate if the next spot is enabled and move there
            const newPosition = validateAndMove(e, position, maze);
            setPosition(newPosition);

            // only count moves if the character moves, avoid counting on other key presses
            if (newPosition !== position) setMoves(state.moves + 1);

            // compare the actual position with the calculated finishing coordinates
            setWinner(compareCoords(newPosition, winningCoords))
        }
    }

    const submitAndRestart = () => {
        setPosition(searchMazeStartEnd(maze, STARTING_POINT_REF));
        submitResults(state.name, state.moves, level);
        restartGame();
    }

    return (
        <div>
            <Header className="game-header" label={GAME_NAME} />

            {/* No name means no submit so Setup is rendered */}
            {!state.name && (
                <SetUp />
            )}

            {/* When name is submitted, the game begins */}
            {state.name && (
                <div className="game-container">
                    {/* Shows the amount of moves or a congrats message if arrived */}
                    <InformationLabel />
                    <Maze position={position} maze={maze} />
                    <div className="bottom-controls">
                        {state.winner && (
                            <Button className="submit-button" action={submitAndRestart} label={SUBMIT_GAME_BUTTON} />
                        )}

                    </div>
                    {state.rankings.length > 0 && (
                        <Rankings level={level} />
                    )}
                </div>
            )}

            {state.error && (
                <span className="error-message">{state.error}</span>
            )}

        </div>
    )
}

export default Game;
