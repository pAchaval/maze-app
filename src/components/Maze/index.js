import React from "react";
import { MAZE_CELL } from "../../constants";
import { compareCoords } from "../../helpers";
import MazeCell from "../MazeCell";

const Maze = ({ position, maze }) => {
    // loop through the length of the 2D array, entering each row of it
    const rows = maze.map((item, i) => {

        //loop through every cell of the arrays inside the multidimensional array
        const entry = item.map((element, j) => {

            // 1/2/3s on the maze represent path or walk able cells
            // 0s represent the walls or unavailable cells
            const backgroundColor = element === 0 ? MAZE_CELL.WALL : MAZE_CELL.PATH;

            // get every cell's unique set of coordinates (index of the row 'i', index of the cell 'j')
            const coords = [i, j];

            // set every cell without character but the one that has matching coords and position 
            let character = false;
            if (compareCoords(position, coords)) { character = true }

            return (
                <MazeCell backgroundColor={backgroundColor} key={j} character={character} />
            );
        });

        return (
            <tr key={i}> {entry} </tr>
        );
    });

    return (
        <div className="table-container">
            <table className="table-maze">
                <tbody>
                    {rows}
                </tbody>
            </table>
        </div>
    );
}

export default Maze;
