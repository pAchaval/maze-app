import React, { useContext, useState } from "react";
import { useEffect } from "react/cjs/react.development";
import { CHARACTER_MOOD } from "../../constants";
import { Context as GameContext } from "../../context/GameContext";

const Character = () => {

    // winning or not is determined by the game, using context to get this info
    const { state } = useContext(GameContext)
    
    // managing locally walking or sleeping mood as its the character's responsability
    const [character, setCharacter] = useState(CHARACTER_MOOD.WALKING);

    useEffect(() => {
    // the character falls asleep after 1.3 secs of inactivity
        setTimeout(() => {
            setCharacter(CHARACTER_MOOD.SLEEPING);
        }, 1300);
    })

    return (
        <>
            {!state.winner && character === CHARACTER_MOOD.WALKING && (
                <span>🧍</span>
            )}
            {!state.winner && character === CHARACTER_MOOD.SLEEPING && (
                <span>😴</span>
            )}
            {state.winner && (
                <span>🤩</span>
            )}
        </>
    )
};

export default Character;
