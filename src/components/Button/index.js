import React from "react";

const Button = ({ label, action, disabled = false, className }) => {
    return <button className={className} onClick={action} disabled={disabled}>{label}</button>
}

export default Button;
