import React from "react";

const Header = ({ label, className }) => {
    return <h1 className={className}>{label}</h1>
};

export default Header;
