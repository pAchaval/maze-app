import React, { useContext } from "react";
import { Context as GameContext } from "../../context/GameContext";

const InformationLabel = () => {
    const { state } = useContext(GameContext);
    const label = state.winner ? `YOU HAVE ARRIVED ON ${state.moves} MOVES!!` : `MOVES: ${state.moves}`
    return <span className="information-label">{label}</span>
}

export default InformationLabel;
