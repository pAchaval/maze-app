import React, { useContext } from "react";
import { RANKING_HEADER } from "../../constants";
import { Context as GameContext } from "../../context/GameContext";

const Rankings = ({ level }) => {
    const { state } = useContext(GameContext);

    return (
        <div className="rankings-table">
            <span className="rankings-header">{RANKING_HEADER} ({level})</span>
            {state.rankings.map((player, i) => {
                return (
                    <tr className={`podium${i + 1}`}>
                        <td>
                            {i + 1} - Name: {player.name} - Moves: {player.moves}
                        </td>
                    </tr>
                )
            })}
        </div>
    )

}

export default Rankings;
