import React from "react";
import Character from "../Character";

const MazeCell = ({ backgroundColor, key, character }) => {
    return (
        <td className="cell-maze" style={{ backgroundColor }} key={key}>
            {character && <Character />}
        </td>
    )
}

export default MazeCell;
