import React from "react";

const Input = ({ placeholder, action, value, className }) => {
    return <input className={className} placeholder={placeholder} onChange={action} value={value} />
};

export default Input;
